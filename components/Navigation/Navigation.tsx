import notifIcon from '../../public/ic_universal/ic-notifications.svg';
import styles from './Navigation.module.css'

function Navigation() {
    return (
        <div className={styles.container}>
            <div>
                <p className={styles.logo}>HSTP.GG</p>
                <div className={styles.menuIcon}>
                    <button className={styles.btn}>N</button>
                    <button className={styles.btn}>M</button>
                </div>
            </div>
        </div>
    )
}

export default Navigation
