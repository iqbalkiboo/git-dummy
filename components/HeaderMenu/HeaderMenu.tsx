import styles from './HeaderMenu.module.css';
import Link from 'next/link';

function HeaderMenu() {
    return (
        <div className={styles.head}>
            <Link href="/">Tim Kamu</Link>
            <Link href="/activity">Aktifitas</Link>
            <Link href="/invitation">Undangan</Link>
            <Link href="/findteam">Cari Tim</Link>
        </div>
    )
}

export default HeaderMenu
