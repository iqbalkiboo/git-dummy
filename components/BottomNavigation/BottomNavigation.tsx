import styles from './BottomNavigation.module.css'

function BottomNavigation() {
    return (
        <div className={styles.botnav}>
            <div className={styles.bg}><button className={styles.btn}>H</button></div>
            <div className={styles.bg}><button className={styles.btn}>R</button></div>
            <div className={styles.bg}><button className={styles.btn}>G</button></div>
            <div className={styles.bg}><button className={styles.btn}>P</button></div>
        </div>
    )
}

export default BottomNavigation
