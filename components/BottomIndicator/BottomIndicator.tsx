import React from 'react';
import styles from './BottomIndicator.module.css'

function BottomIndicator() {
    return (
        <div className={styles.botin}>
            <div className={styles.line}></div>
        </div>
    )
}

export default BottomIndicator
