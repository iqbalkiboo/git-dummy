import styles from '../styles/Home.module.css';
import Navigation from '../components/Navigation/Navigation';
import HeaderMenu from '../components/HeaderMenu/HeaderMenu';
import BottomNavigation from '../components/BottomNavigation/BottomNavigation';
import BottomIndicator from '../components/BottomIndicator/BottomIndicator';

function invitation() {
    return (
        <div className={styles.container}>
            <Navigation />
            <HeaderMenu />
            <h1>Invitation</h1>
            <BottomNavigation />
            <BottomIndicator />
        </div>
    )
}

export default invitation
